package pl.sda.collection;

import java.util.ArrayList;
import java.util.List;

public class ArrayListMain {

    public static void main(String[] args) {

        List<Car> cars = new ArrayList<>(1000);

        Car car1 = new Car(2009, "Opel Insignia", "srebrny");
        Car car2 = new Car(2015, "Kia Ceed", "biały");
        Car car3 = new Car(2015, "Kia Ceed", "biały");


        cars.add(car1);
        cars.add(car2);
        cars.add(car3);

        for (Car c : cars) {
            System.out.println(c.getName());
        }

        System.out.println("Liczba dodanych samchodów: " + cars.size());

        System.out.println("Kolor samochodu o indeksie 1: " + cars.get(1).getColor());

        System.out.println("Czy obiekt istnieje: " + cars.contains(car3));

        System.out.println(cars.remove(car2));

        Car deletedCar = cars.remove(0);

        System.out.println("Usunięto samochód o nazwie: " + deletedCar.getName());

        System.out.println("Liczba samchodów po usunięciu: " + cars.size());

    }

}
