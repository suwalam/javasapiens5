package pl.sda.collection;

import pl.sda.objects.Dog;

public class CarMain {

    public static void main(String[] args) {

        Car car1 = new Car(2009, "Opel Insignia", "srebrny");

        Car car2 = new Car(2015, "Kia Ceed", "biały");

        Car car3 = new Car(2015, "Kia Ceed", "biały");

        boolean isCarsEqual =  car1.equals(car2);

        boolean isCarsEqual2 = car2.equals(car3);

        System.out.println(isCarsEqual);
        System.out.println(car1.hashCode() + " - " + car2.hashCode());
        System.out.println(isCarsEqual2);
        System.out.println(car2.hashCode() + " - " + car3.hashCode());

        car3.setColor("czarny");
        boolean isCarsEqual3 = car2.equals(car3);
        System.out.println("Po aktualizacji car3");
        System.out.println(isCarsEqual3);
        System.out.println(car2.hashCode() + " - " + car3.hashCode());

        boolean isCarAndDogEquals = car3.equals(new Dog("jamnik", "Burek", 11.3, 4));

        System.out.println("Car and dog equals: " + isCarAndDogEquals);
    }

}
