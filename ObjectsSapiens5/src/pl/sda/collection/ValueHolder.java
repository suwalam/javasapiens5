package pl.sda.collection;

public class ValueHolder<T> {

    private T value;

    public ValueHolder(T value) {
        this.value = value;
    }

    public T get() {
        if (value == null) {
            throw new IllegalStateException("Value is null");
        }

        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
