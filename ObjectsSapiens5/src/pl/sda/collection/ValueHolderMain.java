package pl.sda.collection;

public class ValueHolderMain {

    public static void main(String[] args) {

        ValueHolder<Car> carHolder = new ValueHolder<>(new Car(2011, "Opel", "biały"));

        Car car = carHolder.get();

        System.out.println(car.getName());

        carHolder.setValue(null);

        carHolder.get();


    }

}
