package pl.sda.collection;

import com.sun.source.tree.Tree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {

    public static void main(String[] args) {


        Set<String> strings = new HashSet<>();

        strings.add("Ala");
        strings.add("ma");
        strings.add("kota");

        for (String s : strings) {
            System.out.print(s + " ");
        }

        long timeBefore = System.nanoTime();

        strings.contains("Ala");

        long timeAfter = System.nanoTime();

        System.out.println(timeAfter - timeBefore);


        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(10);
        treeSet.add(345645);
        treeSet.add(322);
        treeSet.add(425245425);

        for (Integer i : treeSet) {
            System.out.print(i + " ");
        }

        TreeSet<Car> carTreeSet = new TreeSet<>(new CarComparator());

        Car car1 = new Car(2019, "Opel Insignia", "srebrny");
        Car car2 = new Car(2015, "Kia Ceed", "biały");
        Car car3 = new Car(2021, "Kia Ceed", "biały");
        Car car4 = new Car(2021, "Kia Ceed", "biały");

        carTreeSet.add(car1);
        carTreeSet.add(car2);
        carTreeSet.add(car3);
        carTreeSet.add(car4);

        System.out.println();

        for (Car c : carTreeSet) {
            System.out.print(c.getYear() + " ");
        }




    }

}
