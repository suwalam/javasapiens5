package pl.sda.collection;

import java.util.Objects;

public class Car implements Comparable<Car> {

    private int year;
    private String name;
    private String color;

    public Car(int year, String name, String color) {
        this.year = year;
        this.name = name;
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return year == car.year && name.equals(car.name) && color.equals(car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, name, color);
    }

    @Override
    public int compareTo(Car o) {
        return this.year - o.year; //ascending
    }
}
