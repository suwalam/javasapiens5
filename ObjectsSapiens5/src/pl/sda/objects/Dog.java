package pl.sda.objects;

public class Dog {

    private String breed;

    private String name;

    private double weight;

    private int age;

    public static final int PAW_NUMBER = 4;

    private static int counter;

    public Dog(String breed, String name, double weight, int age) {
        this.breed = breed;
        this.name = name;
        setWeight(weight);
        this.age = age;
        counter++;
    }

    public static int getCounter() {
        return counter;
    }

    public String getBreed() {
        return breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight > 0.0) {
            this.weight = weight;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void bark() {
        if (weight < 10) {
            System.out.println("HAU HAU");
        } else {
            System.out.println("WOW WOW");
        }
    }

}
