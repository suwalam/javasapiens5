package pl.sda.inheritance;


public interface Summarable {

    void summary();

    default void print() {
        System.out.println("Default implementation");
    }

}
