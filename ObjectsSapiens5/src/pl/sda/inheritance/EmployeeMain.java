package pl.sda.inheritance;

public class EmployeeMain {

    public static void main(String[] args) {

        Employee employee = new OfficeEmployee("Jan", "Kowalski", "12345678910", "word");

        Employee officeEmployee =
                new OfficeEmployee("Michał", "Nowak", "12345678911", "excel, word");

        System.out.println(employee.getSurname() + " zarabia " + employee.calculateSalary());

        System.out.println(officeEmployee.getSurname() + " zarabia " + officeEmployee.calculateSalary());

//        officeEmployee.getSkills();

        Employee [] employees = {employee, officeEmployee};

        for(Employee e : employees) {
            e.summary();
        }

        OfficeEmployee officeEmployee2 = new OfficeEmployee("Anna", "Mucha", "12345678912", "excel, word");
        officeEmployee2.print("argument");
        officeEmployee2.print(1111);
        officeEmployee2.print("argument", "argument2", "argument3");
        officeEmployee2.printArray(new String[]{"argument", "argument2", "argument3"});

        System.out.println(officeEmployee2);

    }

}
