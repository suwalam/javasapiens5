package pl.sda.inheritance;

public class OfficeEmployee extends Employee {

    private String skills;

    public OfficeEmployee(String name, String surname, String pesel, String skills) {
        super(name, surname, pesel); //wywołaj konstruktor z klasy Employee
        this.skills = skills;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    @Override
    public double calculateSalary() {

        double salaryBase = 2000;

        if (skills.contains("word")) {
            salaryBase += 500.0;
        }

        if (skills.contains("excel")) {
            salaryBase += 400.0; //salaryBase = salaryBase + 400.0;
        }

        return salaryBase;
    }

    @Override
    public void summary() {
        System.out.println("Imię: " + name + ", nazwisko: " + surname + ", PESEL: " + pesel
        + ", wysokość pensji: " + calculateSalary() + " zł");
    }

    //overloading
    public void print(int x) {
        System.out.println("Wypisuję int: " + x);
    }

    //overloading
    public void print(String x) {
        System.out.println("Wypisuję String: " + x);
    }

    //varargs przyjume zero lub więce j argumentów
    public void print(String... x) {
        for (String s : x) {
            System.out.println("Wypisuję String varargs: " + s);
        }
    }

    public void printArray(String[] x) {
        for (String s : x) {
            System.out.println("Wypisuję String varargs: " + s);
        }
    }

    @Override
    public String toString() {
        return "OfficeEmployee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", skills='" + skills + '\'' +
                '}';
    }
}
