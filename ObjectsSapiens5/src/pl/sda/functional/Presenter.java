package pl.sda.functional;

@FunctionalInterface
public interface Presenter {

    void show(String str);

}
