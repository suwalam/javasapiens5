package pl.sda.functional;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookMain {

    public static void main(String[] args) {

        Set<Book> books = new HashSet<>();
        books.add(new Book("Sienkiewicz", "Ogniem i mieczem", 1234, 1915, 1, "dsgsfdgfd"));
        books.add(new Book("Prus", "Lalka", 994, 1932, 4, "fdsafdsaf"));
        books.add(new Book("Mickiewicz", "Pan Tadeusz", 755, 1874, 7, "dsafadf"));
        books.add(new Book("Orzeszkowa", "Nad Niemnem", 1355, 1945, 8, "safadsf"));
        books.add(new Book("Słowacki", "Kordian", 654, 1925, 1, "adsfadsfadsf"));
        books.add(new Book("Słowacki", "Balladyna", 654, 1920, 1, "adsfadsfadsf"));

//        Consumer<Book> consumer = (b) -> System.out.println(b); //implementaja metody accept
//
//        Book book = new Book("Sienkiewicz", "Ogniem i mieczem", 1234, 1915, 1, "dsgsfdgfd");
//
//        consumer.accept(book);
//
//        books.forEach(consumer);

//        System.out.println("Działanie stream");

        //sortAndPrintBooks(books, consumer);

//        mapAndPrintBooks(books);

        flatMap(new ArrayList<>(books));

    }

    private static void sortAndPrintBooks(Set<Book> books, Consumer<Book> consumer) {
        List<Book> result = books.stream()
                .sorted(   (a, b) -> {
                    int compareResult = a.getAuthor().compareTo(b.getAuthor());

                    if (compareResult == 0) {
                        return a.getYear() - b.getYear();
                    }

                    return compareResult;

                }   )
                .collect(Collectors.toList());

        result.forEach(consumer);
    }

    private static void mapAndPrintBooks(Set<Book> books) {

        books.stream()
                .map(Book::getAuthor)
                .distinct()
                .sorted((a, b) -> b.compareTo(a))
                .forEach(System.out::println);
    }

    private static void flatMap(List<Book> books) {

        Library lib1 = new Library("Warszawa", Arrays.asList(books.get(0), books.get(1), books.get(2)));

        Library lib2 = new Library("Kraków", Arrays.asList(books.get(3), books.get(4), books.get(5)));

        List<Library> libs = new ArrayList<>();
        libs.add(lib1);
        libs.add(lib2);

        Optional<Book> anyBook = libs.stream()
                .map(lib -> lib.getBooks())
                .flatMap(b -> b.stream())
                .filter(b -> b.getAuthor().equals("Słowacki"))
                .findAny();

        if (anyBook.isPresent()) {
            System.out.println(anyBook.get());
        }
    }


}
