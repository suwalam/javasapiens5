package pl.sda.functional;

public class PresenterMain {

    public static void main(String[] args) {

        Presenter presenter = (s) -> System.out.println("Parametr z metody show: " + s);

        presenter.show("Nazwa parametru");

        Presenter presenter2 = (s) -> System.out.println(s.replaceAll("a", "A"));
        presenter2.show("AAAadfsdadgsdgaaaaaaaaaa");


    }

}
