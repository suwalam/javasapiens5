package pl.sda.main;

import pl.sda.objects.Dog;

public class PrzekazywaniePrzezWartoscIReferencjePrzyklad {

    public static void main(String[] args) {

        int a = 5;
        changeVariable(a);
        System.out.println(a);

        Dog dog1 = new Dog("Owczarek", "Reksio", 12.4, 3);

        changeReference(dog1);
        System.out.println(dog1.getAge());

    }

    //przekazywanie przez wartość
    private static void changeVariable(int x) {
        x = 10; //zmień wartość zmiennej x na 10
    }

    //przekazywanie przez referencję
    private static void changeReference(Dog dog) {
        dog.setAge(10);
    }

}
