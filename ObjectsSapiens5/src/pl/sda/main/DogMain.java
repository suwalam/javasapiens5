package pl.sda.main;

import pl.sda.objects.Dog;

public class DogMain {

    public static void main(String[] args) {

        Dog dog1 = new Dog("Owczarek", "Reksio", 12.4, 3);

        dog1.setWeight(-11.4);

        dog1.bark();

        System.out.println("Rasa psa: " + dog1.getBreed());
        System.out.println("Waga psa: " + dog1.getWeight());

        Dog dog2 = new Dog("Jamnik", "Burek", 6.4, 4);

        System.out.println("Pies ma ilość nóg " + Dog.PAW_NUMBER); //poprawnie

        System.out.println("Pies o imieniu " + dog2.getName() + " " + dog2.PAW_NUMBER); //niepoprawnie

        System.out.println("Ile obiektów jest stworzonych: " + Dog.getCounter());

        //Dog.PAW_NUMBER = 5; // nie mozna zmienić wartości stałej

    }

}
