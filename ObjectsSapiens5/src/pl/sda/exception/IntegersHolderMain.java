package pl.sda.exception;

public class IntegersHolderMain {

    public static void main(String[] args) {

        try {
            IntegersHolder holder = new IntegersHolder(3);
            holder.add(10);
            holder.add(2423);
            holder.add(103);
            holder.add(1043);

            //przetestować metodę get(index)

            holder.print();

            holder.removeLast();

            System.out.println("\nPo usunięciu");

            holder.print();

        } catch (IllegalSizeException | HolderOverflowException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Koniec programu");
    }
}
