package pl.sda.exception;

public class HolderOverflowException extends Exception {

    public HolderOverflowException(String message) {
        super(message);
    }
}
