package pl.sda.exception;

public class IllegalIndexException extends Exception {

    public IllegalIndexException(String message) {
        super(message);
    }
}
