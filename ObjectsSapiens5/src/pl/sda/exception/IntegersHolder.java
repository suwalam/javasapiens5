package pl.sda.exception;

public class IntegersHolder {

    private int size;

    private int counter;

    private int [] array;

    public IntegersHolder(int size) throws IllegalSizeException {

        if (size <= 0) {
            throw new IllegalSizeException("Size " + size + " is illegal!");
        }

        this.size = size;
        this.array = new int[size];
    }

    public void add(int element) throws HolderOverflowException {
        if (counter == size) {
            throw new HolderOverflowException("Holder jest pełny!");
        }

        array[counter] = element;
        counter++;
    }

    public int get(int index) throws IllegalIndexException {

        if (index < 0 || index >= size) {
            throw new IllegalIndexException("Index " + index + " jest nieprawidłowy");
        }

        return array[index];
    }

    public void removeLast() {
        if (counter == 0) {
            //rzucamy wyjątek
        }

        counter--;
    }

    public void print() {
        for (int i=0; i<counter; i++) {
            System.out.print(array[i] + " ");
        }
    }

}
