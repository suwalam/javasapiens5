package pl.sda.exception;

public class IllegalSizeException extends Exception {

    public IllegalSizeException(String message) {
        super(message);
    }

}
