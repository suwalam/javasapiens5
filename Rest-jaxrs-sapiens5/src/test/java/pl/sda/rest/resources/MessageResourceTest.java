package pl.sda.rest.resources;

import lombok.extern.java.Log;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import pl.sda.rest.model.Message;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Log
public class MessageResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(MessageResource.class);
    }

    @Test
    public void shouldGetAllMessage() {
        //given
        String endpoint = "/messages";

        //when
        Response response = target(endpoint).request().get(); //metoda HTTP GET

        //then
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assertions.assertNotNull(response.getEntity().toString());

        log.info("Status: " + response.getStatus());
        log.info("Response body: " + response.readEntity(String.class));
    }

    @Test
    public void shouldAddMessage() {
        //given
        Message message = new Message(2, "message-2", LocalDateTime.now(), new ArrayList<>());
        String endpoint = "/messages";

        //when
        Response response = target(endpoint).request().post(
                Entity.entity(message, MediaType.APPLICATION_JSON_TYPE));

        //then
        Assertions.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

}
