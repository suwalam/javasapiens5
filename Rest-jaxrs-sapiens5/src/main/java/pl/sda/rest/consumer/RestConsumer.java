package pl.sda.rest.consumer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class RestConsumer {

    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();
        Response response = client
                .target("http://localhost:8080/rest/api/messages/")
                .request()
                .header("user", "user1")
                .header("password", "pass1")
                .get();

        System.out.println(response.readEntity(String.class));

    }

}
