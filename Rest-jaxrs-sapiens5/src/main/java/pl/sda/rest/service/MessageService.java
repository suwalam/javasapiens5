package pl.sda.rest.service;

import pl.sda.rest.exception.MessageNotFoundException;
import pl.sda.rest.model.Message;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageService {

    private List<Message> messages = new ArrayList<>();

    public MessageService() {

        messages.add(new Message(1, "message-1", LocalDateTime.now(), new ArrayList<>()));
    }

    public void addMessage(Message message) {
        message.setCreateDate(LocalDateTime.now());
        messages.add(message);
    }

    public List<Message> getAllMessages() {
        return messages;
    }

    public void updateMessaage(Message message) {

        int index = getIndexById(message.getId());

        if (index != -1) {
            messages.get(index).setText(message.getText());
        }
    }

    public void deleteMessage(Integer id) {
        Message message = getMessageById(id);

        if (message != null) {
            messages.remove(message);
        }
    }

    public Message getMessageById(Integer id) {

        for (Message message : messages) {
            if (message.getId().equals(id)) {
                return message;
            }
        }

        throw new MessageNotFoundException("Wiadomość o id: " + id + " nie została znaleziona");
    }

    private int getIndexById(Integer id) {
        for (int i=0; i<messages.size(); i++) {
            if (messages.get(i).getId().equals(id)) {
                return i;
            }
        }

        return -1;
    }

}
