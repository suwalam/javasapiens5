package pl.sda.rest.filter;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class AuthorizationRequestFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {

        long headersCount = ctx.getHeaders()
                .entrySet()
                .stream()
                .filter(e -> (e.getKey().equals("user") && e.getValue() != null && !e.getValue().isEmpty() && !e.getValue().get(0).trim().isEmpty()) || (e.getKey().equals("password") && e.getValue() != null && !e.getValue().isEmpty() && !e.getValue().get(0).trim().isEmpty()))
                .count();

        if (headersCount != 2) {
            ctx.abortWith(Response
                    .status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
