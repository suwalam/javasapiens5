package pl.sda.rest.filter;

import lombok.extern.java.Log;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Log
@Provider
public class PrintHeadersRequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {

        MultivaluedMap<String, String> headers = ctx.getHeaders();

                headers
                        .entrySet()
                        .stream()
                        .forEach(e -> log.info(e.getKey() + " - " + e.getValue()));
    }
}
