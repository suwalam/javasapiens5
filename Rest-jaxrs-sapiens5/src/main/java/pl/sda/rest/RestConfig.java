package pl.sda.rest;

import pl.sda.rest.resources.HelloWorldResource;
import pl.sda.rest.resources.MessageResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class RestConfig extends Application {

    private Set<Object> singletons = new HashSet<>();

    public RestConfig() {
        singletons.add(new MessageResource());
        singletons.add(new HelloWorldResource());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
