package pl.sda.rest.resources;

import lombok.extern.java.Log;
import pl.sda.rest.model.Message;
import pl.sda.rest.service.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Arrays;
import java.util.List;

@Log
@Path("/messages")
public class MessageResource {

    private MessageService messageService = new MessageService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMessages() {
        log.info("Return all messages");

        return Response.status(Response.Status.OK)
                .entity(messageService.getAllMessages())
                .build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessageById(@PathParam("id") Integer id) {
        log.info("Return message by id: " + id);
        return Response.status(Response.Status.OK)
                .entity(messageService.getMessageById(id))
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMessage(Message message) {
        log.info("Added message: " + message);
        messageService.addMessage(message);

        log.info("Actual messages size: " + messageService.getAllMessages().size());

        return Response.status(Response.Status.CREATED)
                .build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMessage(Message message) {
        log.info("Updated message: " + message);
        messageService.updateMessaage(message);

        return Response.status(Response.Status.NO_CONTENT)
                .build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMessage(@PathParam("id") Integer id) {
        log.info("Removing message with id: " + id);
        messageService.deleteMessage(id);
        return Response.status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/hateoas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hateoas(@Context UriInfo uriInfo) {
        log.info("Return hateoas message");
        List<Message> result = messageService.getAllMessages();

        //Dla każdej wiadomości uzupełnij link do samej siebie
        result.forEach(message -> initLinks(message, uriInfo));

        Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder())
                .rel("self").build();

        return Response.ok(result)
                .links(self).build();
    }

    @GET
    @Path("/hateoas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hateoasById(@PathParam("id") final Integer id, @Context UriInfo uriInfo) {
        log.info("Return hateoas message by id: " + id);
        Message result = messageService.getMessageById(id);
        Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder())
                .rel("self").build();

        return Response.ok(result).links(self).build();
    }

    private void initLinks(Message message, UriInfo uriInfo) {
        //Utwórz self link
        UriBuilder uriBuilder = uriInfo.getRequestUriBuilder();
        uriBuilder = uriBuilder.path(Integer.toString(message.getId()));
        Link.Builder linkBuilder = Link.fromUriBuilder(uriBuilder);
        Link selfLink = linkBuilder.rel("self").build();
        message.setLinks(Arrays.asList(selfLink));
    }

}
