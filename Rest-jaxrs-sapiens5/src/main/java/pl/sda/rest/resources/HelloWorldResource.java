package pl.sda.rest.resources;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Log
@Path("/hello")
public class HelloWorldResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response hello() {
        log.info("Received GET request from client");

        return Response.status(Response.Status.OK)
        .entity("Hello World!")
                .build();
    }

}
