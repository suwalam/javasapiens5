package pl.sda.rest.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.rest.util.LocalDateTimeDeserializer;
import pl.sda.rest.util.LocalDateTimeSerializer;

import java.time.LocalDateTime;
import java.util.List;

import javax.ws.rs.core.Link;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message {

    private Integer id;

    private String text;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createDate;

    private List<Link> links;
}
