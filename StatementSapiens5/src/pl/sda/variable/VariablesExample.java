package pl.sda.variable; //zawsze pierwsza instrukcja w pliku o rozszerzeniu .java

public class VariablesExample {

    public static void main(String[] args) {

        //deklaracja + inicjalizacja
        byte byteVar = (byte) 100; //-128 - 127

        System.out.println("Zawartość zmiennej byteVar = " + byteVar);

        byteVar = -12;

        var var1 = 100;

        System.out.println("Zawartość zmiennej byteVar po aktualizacji = " + byteVar);

        short shortVar = 32222;

        int intVar = 24134134;

        long longVar = 214134134134134134L; //zawsze umieszczamy na końcu literę L
        long longVar2 = 2_345_234_524_543_224L;
        long longSum = longVar + longVar2;

        System.out.println("Wynik sumowania: " + longSum);

        float floatVar = 13.9999999F;

        System.out.println("Wynik mnożenia = " + floatVar * 2);

        double doubleVar = 24.996457457435745;

        int intVar2 = (int) doubleVar;

        System.out.println("Zawartość zmiennej intVar po aktualizacji = " + intVar2);

        boolean boolVar = true;

        char charVar = 'a';

        charVar = 10;

        System.out.println("Zawartość zmiennej charVar = " + charVar + " !");

        int intVar3 = 3;
        intVar3 *= 5;  //varInt = varInt * 5;

        System.out.println("Zawartość zmiennej intVar3 = " + intVar3);

    }

}
