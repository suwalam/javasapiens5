package pl.sda.variable;

import java.util.Scanner;

public class InputOutputExamples {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj swoje imię: ");

        String name = scanner.next();

        System.out.println("Witaj, " + name);

        System.out.println("Podaj swój wiek: ");

        int age = scanner.nextInt();

        System.out.println("Twój wiek to " + age);


        if (age >= 0 && age < 18) {
            System.out.println("Jesteś niepełnoletni!");
        } else if (age >= 18) {
            System.out.println("Jesteś pełnoletni!");
        } else { //w każdym innym przypadku
            System.out.println("Podana wartość jest ujemna");
        }


        System.out.println("Podaj numer miesiąca: ");

        //inicjalizacja zmiennej
        byte monthNumber = scanner.nextByte();

        switch (monthNumber) {
            case 1:
                System.out.println("Styczeń"); break;
            case 2:
                System.out.println("Luty"); break;
            case 3:
                System.out.println("Marzec"); break;
            case 4:
                System.out.println("Kwiecień"); break;

            default:
                System.out.println("Wartość nieobsługiwana"); break;
        }


        scanner.close();


    }

}
