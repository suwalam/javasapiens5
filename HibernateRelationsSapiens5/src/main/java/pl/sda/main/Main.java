package pl.sda.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.model.Address;
import pl.sda.model.Client;
import pl.sda.model.Order;
import pl.sda.model.Person;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        /*Address address = new Address(null, "Warszawa", "Puławska 123", null);

        Person person = new Person(null, "Jan", "Kowalski", LocalDate.now(), address);

        address.setPerson(person);

        session.save(address);
        session.save(person);

        Person personFromDB = session.get(Person.class, 1L);
        System.out.println(personFromDB);

        //relacja dwustronna
        Address addressFromDB = session.get(Address.class, 1L);
        System.out.println(addressFromDB);
        System.out.println(addressFromDB.getPerson());*/

        Order order1 = new Order(null, new BigDecimal(100.26), "Książki", LocalDateTime.now(), null);
        Order order2 = new Order(null, new BigDecimal(10075.81), "Napoje", LocalDateTime.now(), null);

        Client client = new Client(null, "Jan", "Kowalski", Arrays.asList(order1, order2));

        order1.setClient(client);
        order2.setClient(client);

        session.save(order1);
        session.save(order2);
        session.save(client);

        Client clientFromDB = session.get(Client.class, 1L);
        System.out.println(clientFromDB);

        transaction.commit();
        session.close();
        sessionFactory.close();


    }

}
