package pl.sda.overriding;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


public class SubClass extends SuperClass {

    public SubClass(int index, int index2) {
        super(index);
    }

    // modyfikatory widocznośći - mozna rozszerzać
    // typ zwracany - można zawężać
    // lista wyjątków - można zawężać
    @Override
    public ArrayList<String> method() throws FileNotFoundException {
        return null;
    }
}
